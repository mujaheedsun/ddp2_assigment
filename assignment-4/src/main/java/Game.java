import java.awt.Dimension;
import javax.swing.JFrame;

// main method
public class Game {
    public static void main(String[] args) {
        Board b = new Board();
        b.setPreferredSize(new Dimension(900,700)); //need to use this instead of setSize
        b.setLocation(250, 50);
        b.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        b.pack();
        b.setVisible(true);
        b.setResizable(false);
    }   
}