import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Board extends JFrame {
    private JPanel panelCounter;
    private JPanel panelButton;

    private Card selectedCard;
    private Card c1;
    private Card c2;
    private Timer timer;
    
    private ArrayList<Card> cardsList = new ArrayList<Card>();
    private ArrayList<String> cardsString = new ArrayList<String>();

    private JButton playAgain;
    private JButton exit;
    private JLabel counterText;
    private static int counterInt = 0;
    private static final int PAIRS = 18;

    /**
    * Constructor for Board Class.
    * @return void, Will just add a constructor into Board() class
    */
    public Board() {

        for (int i = 1; i <= PAIRS; i++) {
            cardsString.add("img/img-" + i + ".jpg");
            cardsString.add("img/img-" + i + ".jpg");
        }

        Collections.shuffle(cardsString);

        for (String val : cardsString) {
            Card c = new Card(new ImageIcon(val));
            c.setId(val);
            c.setIcon(Card.cardCover);
            c.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    selectedCard = c;
                    doTurn();
                }
            });
            cardsList.add(c);
        }

        //set up the timer
        timer = new Timer(750, new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                checkCards();
            }
        });

        timer.setRepeats(false);

        panelButton = new JPanel(new GridLayout(6,6));
        for (Card c : cardsList) {
            panelButton.add(c);
        }

        panelCounter = new JPanel(new FlowLayout());
        counterText = new JLabel("Number of tries: " + counterInt);
        playAgain = new JButton("Play Again?");
        exit = new JButton("Exit");
        panelCounter.add(counterText);
        panelCounter.add(playAgain);
        panelCounter.add(exit);

        playAgain.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                doPlayAgain();
            }
        });

        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                System.exit(1);
            }
        });

        add(panelButton, BorderLayout.CENTER);
        add(panelCounter, BorderLayout.SOUTH);

        setTitle("Memory Match-Ikhsanul Akbar Rasyid");
    }

    /**
     * doTurn() method will do when button clicked
     * @return void, Just modify instance variable's values 
     */
    public void doTurn() {
        
        if (c1 == null && c2 == null) {
            c1 = selectedCard;
            c1.setIcon(c1.getCardIcon());
            counterInt++;
        }

        if (c1 != null && c1 != selectedCard && c2 == null) {
            c2 = selectedCard;
            c2.setIcon(c2.getCardIcon());
            timer.start();
        }

        counterText.setText("Number of tries: " + counterInt);
    }

    /**
     * checkCards() method will check the button 
     * @return void, Just modify instance variable's values 
     */
    public void checkCards() {
        //match condition
        if (c1.getId().equals(c2.getId())) {
            c1.setEnabled(false); //disables the button
            c2.setEnabled(false);
            c1.setVisible(false);
            c2.setVisible(false);
            c1.setMatched(true); //flags the button as having been matched
            c2.setMatched(true);
            if (this.isGameWon()) {
                JOptionPane.showMessageDialog(this, "You win!");
            }
        }

        else {
            c1.setText(""); //'hides' text
            c2.setText("");
            c1.setIcon(new ImageIcon("img/img-20.jpg"));
            c2.setIcon(new ImageIcon("img/img-20.jpg"));
        }
        c1 = null; //reset c1 and c2
        c2 = null;
    }

    /**
     * isGameWon() method will return true if all is matched
     * @return boolean, Just modify instance variable's values 
     */
    public boolean isGameWon() {
        for(Card c: this.cardsList) {
            if (c.getMatched() == false) {
                return false;
            }
        }
        return true;
    }

    /**
     * doPlayAgain() method will restart the game
     * @return void, Just modify instance variable's values 
     */
    public void doPlayAgain() {
        panelButton.removeAll();
        Collections.shuffle(cardsList);

        // to set the cardButton        
        for (Card val : cardsList) {
            if (val.getMatched() == true) {
                val.setMatched(false);
            } 
            val.setIcon(Card.cardCover);
            val.setEnabled(true);
            val.setVisible(true);
            panelButton.add(val);
        }

        revalidate();
        repaint();

        counterInt = 0;
        counterText.setText("Number of tries: " + counterInt);
    }
}