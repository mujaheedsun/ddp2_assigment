import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Card extends JButton {
    private String id;
    private boolean matched = false;
    private Icon cardIcon;
    public static final Icon cardCover = new ImageIcon("img/img-20.jpg");

    /**
    * Constructor for Card class.
    * @return void, Will just add a constructor into Card() class
    */
    public Card(Icon cardIcon) {
        this.cardIcon = cardIcon;
    }

    /**
	 * @param String the id
	 */
	public void setId(String id) {
        this.id = id;
    }

    /**
     * This method gets the type of the ID
     * @return String, Just modify instance variable's values 
     */
    public String getId() {
        return this.id;
    }

    /**
	 * @param boolean the condition of card
	 */
    public void setMatched(boolean matched) {
        this.matched = matched;
    }

    /**
     * This method gets the condition of card
     * @return boolean, Just modify instance variable's values 
     */
    public boolean getMatched() {
        return this.matched;
    }

	/**
	 * @return the cardIcon
	 */
	public Icon getCardIcon() {
		return this.cardIcon;
	}

	/**
	 * @param cardIcon the cardIcon to set
	 */
	public void setCardIcon(Icon cardIcon) {
        this.cardIcon = cardIcon;
    }
}