import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    // You can add new variables or methods in this class

    public static void main(String[] args) {
        // TODO Complete me!
    	Scanner input = new Scanner(System.in);
    	int n = Integer.parseInt(input.nextLine());
    	TrainCar[] kereta = new TrainCar[n];

    	int count = 0;
    	double bmiAverage = 0;
    	for (int i = 0; i < n; i++) {
    		String[] command = input.nextLine().split(",");
    		WildCat kucing = new WildCat(command[0],Double.parseDouble(command[1]), Double.parseDouble(command[2]));
    		if (count == 0) kereta[i] = new TrainCar(kucing);
    		else {
    			kereta[i] = new TrainCar(kucing, kereta[i-1]);
    		}
    		count ++;

    		if ((kereta[i].computeTotalWeight() >= THRESHOLD) || (i == n-1)){
    			System.out.print("The train departs to Javari Park\n[LOCO]<--");
    			kereta[i].printCar();
    			bmiAverage = kereta[i].computeTotalMassIndex() / count;
    			System.out.println("Average mass index of all cats: " + bmiAverage);
    			System.out.print("In average, the cats in the train are ");
    			if (bmiAverage < 18.5) System.out.println("underweight");
    			else if (18.5 <= bmiAverage && bmiAverage < 25) System.out.println("normal");
    			else if (25 <= bmiAverage && bmiAverage < 30) System.out.println("overweight");
    			else System.out.println("obese");
    			count = 0;
    		}

    	}





    }
}
