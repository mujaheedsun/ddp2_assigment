public class TrainCar {

    WildCat cat;
    TrainCar next;

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    // TODO Complete me!

    public TrainCar(WildCat cat) {
        // TODO Complete me!
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        // TODO Complete me!
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        // TODO Complete me!
        if (this.next == null) {
            return EMPTY_WEIGHT + cat.weight;
        } else {
            return EMPTY_WEIGHT + cat.weight + next.computeTotalWeight();
        }
    }

    public double computeTotalMassIndex() {
        // TODO Complete me!
        if (this.next == null) {
            return cat.computeMassIndex();
        } else {
            return cat.computeMassIndex() + next.computeTotalMassIndex();
        }
    }

    public void printCar() {
        // TODO Complete me!
        if (this.next == null) {
            System.out.println("(" + cat.name + ")");
        } else {
            System.out.print("(" + cat.name + ")--");
            next.printCar();
        }
    }
}
