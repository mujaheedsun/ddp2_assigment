package javari.repository;

import javari.animal.Animal;

import java.util.ArrayList;
import java.util.List;

public class RecordRepository {
    private static ArrayList<Animal> animals = new ArrayList<>();

    public static void save(Animal animal) throws UnsupportedOperationException {
        if (RecordRepository.findByIdNotNull(animal.getId())) {
            throw new UnsupportedOperationException();
        }
        animals.add(animal);

    }

    public static List<Animal> getAnimalsBySpecies(String species) {
        ArrayList<Animal> ret = new ArrayList<>();
        for (Animal animal : animals) {
            if (animal.getClass().getSimpleName().equals(species)) ret.add(animal);
        }
        return ret;
    }

    public static String getAnimalsNameBySpeciesAsStrings(String species) {
        List<Animal> animals = getAnimalsBySpecies(species);
        StringBuilder ret = new StringBuilder();
        for (Animal animal : animals) {
            ret.append(animal.getName());
            ret.append(", ");
        }
        return ret.toString().substring(0, ret.toString().length() - 2);
    }

    public static List<Animal> getAnimals() {
        return animals;
    }

    public static long getNumberOfAnimals() {
        return getAnimals().size();
    }

    public static Animal findById(long id) {
        for (Animal animal : animals) {
            if (animal.getId() == id) {
                return animal;
            }
        }
        return null;
    }

    public static boolean findByIdNotNull(long id) {
        return findById(id) != null;
    }
}
