package javari.repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CategoriesRepository {
    private static ArrayList<String> datas = new ArrayList<>();

    public static void save(String data) {
        datas.add(data);
    }

    public static long findNumberOfSection() {
        return findAllSection().size();
    }

    public static List<String> findAllSection() {
        Set<String> ret = new HashSet<>();
        for (String data : datas) {
            ret.add(data.split(",")[2]);
        }
        return new ArrayList<>(ret);
    }

    public static String findSectionById(int id) {
        return findAllSection().get(id - 1);
    }

    public static long findNumberOfCategories() {
        return findAllCategories().size();
    }

    public static List<String> findAllCategories() {
        Set<String> ret = new HashSet<>();
        for (String data : datas) {
            ret.add(data.split(",")[1]);
        }
        return new ArrayList<>(ret);
    }

    public static List<String> findAllSpecies() {
        Set<String> ret = new HashSet<>();
        for (String data : datas) {
            ret.add(data.split(",")[0]);
        }
        return new ArrayList<>(ret);
    }

    public static List<String> findAllSpeciesBySection(String section) {
        Set<String> ret = new HashSet<>();
        for (String data : datas) {
            if (data.split(",")[2].equals(section)) {
                ret.add(data.split(",")[0]);
            }
        }
        return new ArrayList<>(ret);
    }

    public static String findSpeciesBySectionAndId(String section, int id) {
        List<String> species = findAllSpeciesBySection(section);
        return species.get(id - 1);
    }


}
