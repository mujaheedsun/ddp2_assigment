package javari.animal.kelas;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Aves extends Animal {
    private SpecialCondition specialCondition;

    public Aves(Integer id, String name, Gender gender, double length,
                double weight, SpecialCondition specialCondition, Condition condition) {
        super(id, name, gender, length, weight, condition);
        this.specialCondition = specialCondition;
    }

    protected boolean specificCondition() {
        return specialCondition == SpecialCondition.NOT_LAYING_EGGS;
    }

    public enum SpecialCondition {
        LAYING_EGGS, NOT_LAYING_EGGS;
        private static final String LAYING_EGGS_STR = "laying eggs";
        private static final String NOT_LAYING_EGGS_STR = "";

        public static SpecialCondition parseSpecialCondition(String str) {
            if (str.equalsIgnoreCase(LAYING_EGGS_STR)) {
                return SpecialCondition.LAYING_EGGS;
            } else if (str.equalsIgnoreCase(NOT_LAYING_EGGS_STR)) {
                return SpecialCondition.NOT_LAYING_EGGS;
            } else {
                throw new UnsupportedOperationException();
            }
        }
    }
}
