package javari.animal.kelas;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Mammals extends Animal {
    private SpecialCondition specialCondition;

    public Mammals(Integer id, String name, Gender gender, double length,
                   double weight, SpecialCondition specialCondition, Condition condition) {
        super(id, name, gender, length, weight, condition);
        this.specialCondition = specialCondition;
    }

    protected boolean specificCondition() {
        return specialCondition == SpecialCondition.NOT_PREGNANT;
    }

    public enum SpecialCondition {
        PREGNANT, NOT_PREGNANT;
        private static final String PREGNANT_STR = "pregnant";
        private static final String NOT_PREGNANT_STR = "";

        public static SpecialCondition parseSpecialCondition(String str) {
            if (str.equalsIgnoreCase(PREGNANT_STR)) {
                return SpecialCondition.PREGNANT;
            } else if (str.equalsIgnoreCase(NOT_PREGNANT_STR)) {

                return SpecialCondition.NOT_PREGNANT;
            } else {
                throw new UnsupportedOperationException();
            }
        }
    }
}
