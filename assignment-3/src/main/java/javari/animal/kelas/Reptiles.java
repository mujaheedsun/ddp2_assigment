package javari.animal.kelas;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Reptiles extends Animal {
    private SpecialCondition specialCondition;

    public Reptiles(Integer id, String name, Gender gender, double length,
                    double weight, SpecialCondition specialCondition, Condition condition) {
        super(id, name, gender, length, weight, condition);
        this.specialCondition = specialCondition;
    }

    protected boolean specificCondition() {
        return specialCondition == SpecialCondition.TAME;
    }

    public enum SpecialCondition {
        TAME, WILD;
        private static final String TAME_STR = "tame";
        private static final String WILD_STR = "wild";

        public static SpecialCondition parseSpecialCondition(String str) {
            if (str.equalsIgnoreCase(TAME_STR)) {
                return SpecialCondition.TAME;
            } else if (str.equalsIgnoreCase(WILD_STR)) {
                return SpecialCondition.TAME;
            } else {
                throw new UnsupportedOperationException();
            }
        }
    }
}
