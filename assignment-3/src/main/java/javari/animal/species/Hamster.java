package javari.animal.species;

import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.kelas.Mammals;

public class Hamster extends Mammals {
    public Hamster(Integer id, String name, Gender gender, double length,
                   double weight, SpecialCondition specialCondition, Condition condition) {
        super(id, name, gender, length, weight, specialCondition, condition);
    }
}
