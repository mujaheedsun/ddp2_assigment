package javari.animal.species;

import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.kelas.Reptiles;

public class Snake extends Reptiles {
    public Snake(Integer id, String name, Gender gender, double length,
                 double weight, SpecialCondition specialCondition, Condition condition) {
        super(id, name, gender, length, weight, specialCondition, condition);
    }
}
