package javari.animal.species;

import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.kelas.Mammals;

public class Lion extends Mammals {
    public Lion(Integer id, String name, Gender gender, double length,
                double weight, SpecialCondition specialCondition, Condition condition) {
        super(id, name, gender, length, weight, specialCondition, condition);
    }

    @Override
    protected boolean specificCondition() {
        return super.specificCondition() && this.body.getGender() == Gender.MALE;
    }
}
