package javari.park;

import javari.animal.Animal;

import java.util.ArrayList;
import java.util.List;

public class Attraction implements SelectedAttraction {

    private static ArrayList<Attraction> attractionsList = new ArrayList<>();

    private String name;
    private String type;
    private List<Animal> performers = new ArrayList<>();

    public Attraction() {
    }

    public Attraction(String name, String type) {
        this.name = name;
        this.type = type;
        attractionsList.add(this);
    }

    public static Attraction findAttraction(String name, String type) {
        for (Attraction attraction : attractionsList) {
            if (attraction.getName().equals(name) && attraction.getType().equals(type))
                return attraction;
        }
        return null;
    }

    public static ArrayList<Attraction> findAttractions(String type) {
        ArrayList<Attraction> array = new ArrayList<>();
        for (Attraction attraction : attractionsList) {
            if (attraction.getType().equals(type))
                array.add(attraction);
        }
        return array;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public List<Animal> getPerformers() {
        return performers;
    }

    public boolean addPerformer(Animal performer) {
        if (performer.isShowable()) {
            performers.add(performer);
            return true;
        }
        return false;
    }

    public ArrayList<Attraction> getAttractionsList() {
        return attractionsList;
    }
}
