package javari.reader;

import javari.repository.AttractionsRepository;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;

public class AnimalAttractionsReader extends CsvReader {
    private static final String validInputs[] = {
            "Whale,Circles of Fires",
            "Lion,Circles of Fires",
            "Eagle,Circles of Fires",
            "Cat,Dancing Animals",
            "Snake,Dancing Animals",
            "Parrot,Dancing Animals",
            "Hamster,Dancing Animals",
            "Hamster,Counting Masters",
            "Whale,Counting Masters",
            "Parrot,Counting Masters",
            "Cat,Passionate Coders",
            "Hamster,Passionate Coders",
            "Snake,Passionate Coders"
    };

    public AnimalAttractionsReader(Path file) throws IOException {
        super(file);
    }

    public long countValidRecords() {
        long ret = 0;
        for (String line : lines) {
            if (Arrays.asList(validInputs).contains(line)) {
                ret++;
                AttractionsRepository.save(line);
            }
        }
        return ret;
    }

    public long countInvalidRecords() {
        long ret = countValidRecords();
        return lines.size() - ret;
    }
}
