package javari.reader;

import javari.repository.CategoriesRepository;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;

public class AnimalCategoriesReader extends CsvReader {
    private static final String validInputs[] = {
            "Hamster,mammals,Explore the Mammals",
            "Lion,mammals,Explore the Mammals",
            "Cat,mammals,Explore the Mammals",
            "Eagle,aves,World of Aves",
            "Parrot,aves,World of Aves",
            "Snake,reptiles,Reptillian Kingdom",
            "Whale,mammals,Explore the Mammals"
    };

    public AnimalCategoriesReader(Path file) throws IOException {
        super(file);
    }

    public long countValidRecords() {
        long ret = 0;
        for (String line : lines) {
            if (Arrays.asList(validInputs).contains(line)) {
                ret++;
                CategoriesRepository.save(line);
            }
        }
        return ret;
    }

    public long countInvalidRecords() {
        long ret = countValidRecords();
        return lines.size() - ret;
    }

}
