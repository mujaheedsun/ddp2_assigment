package cage;

import animal.Animal;

public class Cage{
    
    private Animal hewan;
    private String cageType;    // ini A B C
    private String inoutdoor;   // ini indoor outdoor

    public Cage(Animal hewan){
        this.hewan = hewan;
        setInoutdoor();
        setCageType();
    }

    public Animal getHewan() {
		return hewan;
	}

	public String getCageType() {
		return cageType;
	}

	public void setCageType() {
		if (this.inoutdoor.equals("Indoor")){
            if (hewan.getBodyLength() < 45) this.cageType = "A";
            else if (hewan.getBodyLength() > 60) this.cageType = "C";
            else this.cageType = "B";
	    } else {
            if (hewan.getBodyLength() < 75) this.cageType = "A";
            else if (hewan.getBodyLength() > 90) this.cageType = "C";
            else this.cageType = "B";
        }
        
    }

	public String getInoutdoor() {
		return inoutdoor;
	}

	public void setInoutdoor() {
        if (hewan.isPets()){
            this.inoutdoor = "Indoor";
        } else { 
            this.inoutdoor = "Outdoor";
        }
	}

    
}