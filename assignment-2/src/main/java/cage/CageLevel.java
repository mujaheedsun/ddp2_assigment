package cage;
import java.util.ArrayList;

public class CageLevel{

    private int size;
    private ArrayList<Cage> level1 = new ArrayList<Cage>();
    private ArrayList<Cage> level2 = new ArrayList<Cage>();
    private ArrayList<Cage> level3 = new ArrayList<Cage>();
    private String inoutdoor;

    public CageLevel(int size){
        this.size = size;
    }

    public void insert(Cage kandang, int idx) {
        int hasilBagi = size / 3;
        int sisaBagi = size % 3;
        if (idx == 0) setInoutdoor(kandang.getInoutdoor());
        if (hasilBagi == 0){
            hasilBagi = 1;
        }
        if (idx < hasilBagi) {
            level1.add(kandang);
        } else if (idx < 2 * hasilBagi) {
            level2.add(kandang);
        } else 
            level3.add(kandang);
    }

    public Cage find(String namaHewan){
        ArrayList<Cage> checked;
        Cage direturn = null;
        for (int i = 0; i < 3; i++){
            if (i == 0) checked = level1;
            else if (i == 1) checked = level2;
            else checked = level3;

            for (Cage dicek : checked) {
                if (dicek.getHewan().getName().equals(namaHewan)) direturn = dicek;
            }
        }
        return direturn;
    }

	public String getInoutdoor() {
		return inoutdoor;
	}

    public void setInoutdoor(String inoutdoor) {
		this.inoutdoor = inoutdoor;
    }
    
    /**
     * @return the level1
     */
    public ArrayList<Cage> getLevel1() {
        return level1;
    }

    /**
     * @return the level2
     */
    public ArrayList<Cage> getLevel2() {
        return level2;
    }

    /**
     * @return the level3
     */
    public ArrayList<Cage> getLevel3() {
        return level3;
    }

    public int getSize() {
        return size;
    }
    
    /**
     * @param level1 the level1 to set
     */
    public void setLevel1(ArrayList<Cage> level1) {
        this.level1 = level1;
    }

    /**
     * @param level2 the level2 to set
     */
    public void setLevel2(ArrayList<Cage> level2) {
        this.level2 = level2;
    }

    /**
     * @param level3 the level3 to set
     */
    public void setLevel3(ArrayList<Cage> level3) {
        this.level3 = level3;
    }
    
}
