package cage;

import java.util.ArrayList;

public class CageArrange{

    public static void arrange(CageLevel hehe){
        System.out.println("location: " + hehe.getInoutdoor());
        print(hehe);

        ArrayList<Cage> temp = hehe.getLevel1();
        hehe.setLevel1(hehe.getLevel3());
        hehe.setLevel3(hehe.getLevel2());
        hehe.setLevel2(temp);

        for (int i=0; i < 3; i++){
            ArrayList<Cage> printed;
            if (i == 0) printed = hehe.getLevel3();
            else if (i == 1) printed = hehe.getLevel2();
            else printed = hehe.getLevel1();

            for (int j = 0; j < printed.size()/2 ; j++) {
                Cage temp_ = printed.get(j);
                printed.set(j, printed.get(printed.size()-j-1));
                printed.set((printed.size()-j-1), temp_);
            }
        }
        System.out.println("After rearrangement...");
        print(hehe);
        
    }

    public static void print(CageLevel hehe) {
        ArrayList<Cage> printed;
        for (int i = 3; i > 0; i--){
            if (i == 3) printed = hehe.getLevel3();
            else if (i == 2) printed = hehe.getLevel2();
            else printed = hehe.getLevel1();
            
            System.out.print("Level " + i + ": ");
            for (Cage diprint : printed) {
                String nama = diprint.getHewan().getName();
                int panjang = diprint.getHewan().getBodyLength();
                String tipe = diprint.getCageType();

                System.out.print(nama + " (" + panjang + " - " + tipe + "), ");
            }
            System.out.println();
        }

        
    }

}