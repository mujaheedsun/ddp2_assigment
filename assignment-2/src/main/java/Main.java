import java.util.Scanner;

import javax.swing.plaf.synth.SynthOptionPaneUI;

import animal.Animal;
import animal.Cat;
import animal.Eagle;
import animal.Hamster;
import animal.Lion;
import animal.Parrot;
import cage.Cage;
import cage.CageArrange;
import cage.CageLevel;

import java.util.ArrayList;

public class Main{

    public static void main(String[] args){

        String[] jenis2Hewan = {"cat", "eagle", "hamster", "parrot", "lion"};

        CageLevel[] kandang = new CageLevel[jenis2Hewan.length];

        Scanner input = new Scanner(System.in);
        
        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");

        for (int i=0; i < 5; i++){
            System.out.print(jenis2Hewan[i] + ": ");
            int banyak = Integer.parseInt(input.nextLine());
            kandang[i] = new CageLevel(banyak);
            if (banyak == 0) continue;
            
            System.out.println("Provide information of" + jenis2Hewan[i] + "(s):");
            String[] masukan = input.nextLine().split(",");
            int idx = 0;

            for (String ambil : masukan) {
                Animal hewan2 = null;
                

                String[] masukanLagi = ambil.split("\\|");
                if (jenis2Hewan[i].equals("cat")) {
                    hewan2 = new Cat(masukanLagi[0], Integer.parseInt(masukanLagi[1]));
                } else if (jenis2Hewan[i].equals("eagle")) {
                    hewan2 = new Eagle(masukanLagi[0], Integer.parseInt(masukanLagi[1]));
                } else if (jenis2Hewan[i].equals("hamster")) {
                    hewan2 = new Hamster(masukanLagi[0], Integer.parseInt(masukanLagi[1]));
                } else if (jenis2Hewan[i].equals("lion")) {
                    hewan2 = new Lion(masukanLagi[0], Integer.parseInt(masukanLagi[1]));
                } else if (jenis2Hewan[i].equals("parrot")) {
                    hewan2 = new Parrot(masukanLagi[0], Integer.parseInt(masukanLagi[1]));
                }

                Cage kandangHomo = new Cage(hewan2);
                kandang[i].insert(kandangHomo, idx);

                idx++;
            }  
        }

        System.out.println("Animals has been successfully recorded!");
        System.out.println("=============================================\nCage arrangement:");

        for (int i=0; i < kandang.length ; i++){
            if (kandang[i].getSize() == 0) continue;
            CageArrange.arrange(kandang[i]);
        }

        System.out.println("ANIMALS NUMBER:");
        for(int i=0; i < jenis2Hewan.length; i++){
            System.out.println(jenis2Hewan[i] + ":" + kandang[i].getSize());
        }

        System.out.println("\n=============================================");

        while(true){
            System.out.println("Which animal do you want to visit?\n(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)");
            int n = Integer.parseInt(input.nextLine());
            if (n == 99) break;
            System.out.print("Mention the name of " + jenis2Hewan[n-1] + " you want to visit: ");
            String namaHewan = input.nextLine();

            Cage aaaaa = kandang[n-1].find(namaHewan);

            if (aaaaa == null) {
                System.out.println("There is no " + jenis2Hewan[n-1] +" with that name!");
                continue;
            }
            System.out.println("You are visiting " + aaaaa.getHewan().getName() + " (" + jenis2Hewan[n-1] +") now, what would you like to do?");

            aaaaa.getHewan().mauapa();
            aaaaa.getHewan().action();

            System.out.println("\nBack to the office!");

        }
    }
}