package animal;

import java.util.Scanner;

public class Eagle extends Animal{
    
    public Eagle(String nama, int panjang){
        super(nama, panjang, false, "eagle");
    }

    public void mauapa(){
        System.out.println("1: Order to fly");                       // ini menanyakan perlakuan yg mau dilakukan visitor thd hewan
    }

    public void orderToFly(){
        makeVoice("kwaakk..\nYou hurt!");
    }

    public void action(){
        Scanner input = new Scanner(System.in);
        int masukan = Integer.parseInt(input.nextLine());
        if (masukan == 1) orderToFly();
        else doNothing();
    }
}