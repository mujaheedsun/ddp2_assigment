package animal;

public class Animal{
    
    protected String name;
    protected int bodyLength;
    protected boolean pets;
	private String jenis;

    public Animal(String nama, int panjang, boolean type, String jenis){
        this.name = nama;
        this.bodyLength = panjang;
        this.pets = type;
        this.jenis = jenis;
    }

    public String getName() {
        return name;
    }

    public int getBodyLength() {
        return bodyLength;
    }

    public boolean isPets() {
        return pets;
    }

    public void mauapa(){
        System.out.print("");
    }

    public void makeVoice(String kata){
        System.out.println(name + " makes a voice: " + kata);
    }

    public void action(){
        System.out.print("");
    }

    public void doNothing(){
        System.out.println("You do Nothing");
    }
    
}