package animal;

import java.util.Scanner;

public class Hamster extends Animal{
    
    public Hamster(String name, int length){
        super(name, length, true, "hamster");
    }

    public void mauapa(){
        System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");                       // ini menanyakan perlakuan yg mau dilakukan visitor thd hewan
    }

    public void gnawing(){
        makeVoice("ngkkrit.. ngkkrrriiit");
    }

    public void runWheel(){
        makeVoice("trrr... trrr...");
    }

    public void action(){
        Scanner input = new Scanner(System.in);
        int masukan = Integer.parseInt(input.nextLine());
        if (masukan == 1) gnawing();
        else if (masukan == 2) runWheel();
        else doNothing();
    }
}