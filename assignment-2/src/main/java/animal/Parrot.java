package animal;

import java.util.Scanner;

public class Parrot extends Animal{

    public Parrot(String nama, int panjang){
        super(nama, panjang, true, "parrot");
    }

    public void mauapa(){
        System.out.println("1: Order to fly 2: Do conversation");      // ini menanyakan perlakuan yg mau dilakukan visitor thd hewan
    }

    public void orderFly(){
        System.out.printf("Parrot %s flies!", this.name);
        makeVoice("FLYYYY.....");
    }

    public void doConversation(){
        Scanner input = new Scanner(System.in);
        String omongan = input.nextLine();
        makeVoice(omongan.toUpperCase());
    }

    public void action(){
        Scanner input = new Scanner(System.in);
        int masukan = Integer.parseInt(input.nextLine());
        if (masukan == 1) orderFly();
        else if (masukan == 2) doConversation();
        else doNothing();
    }
}