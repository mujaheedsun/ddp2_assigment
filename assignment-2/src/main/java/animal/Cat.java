package animal;
import java.util.Scanner;
import java.util.Random;

public class Cat extends Animal{

    
    Random rand = new Random();

    public Cat(String name, int length){
        super(name, length, true, "cat");
    }


    public void mauapa(){
        System.out.println("1: Brush the fur 2: Cuddle");                       // ini menanyakan perlakuan yg mau dilakukan visitor thd hewan
    }

    public void brush(){
        System.out.printf("Time to clean %s's fur\n", name);
        makeVoice(" Nyaaan...");
    }
    
    public void cuddle(){
        int n = rand.nextInt(4) + 1;
        String suara;
        if (n == 1) suara = "Miaaaw..";
        else if (n == 2) suara = "Purrr..";
        else if (n == 3) suara = "Mwaw!";
        else suara = "Mraaawr!";
        makeVoice(suara);
    }

    public void action(){
        Scanner input = new Scanner(System.in);
        int masukan = Integer.parseInt(input.nextLine());

        if (masukan == 1) brush();
        else if (masukan == 2) cuddle();
        else doNothing();
    }
    
    

}