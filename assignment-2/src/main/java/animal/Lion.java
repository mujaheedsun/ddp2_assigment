package animal;

import java.util.Scanner;

public class Lion extends Animal{
    
    public Lion(String nama, int panjang){
        super(nama, panjang, false, "lion");
    }

    public void mauapa(){
        System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");      // ini menanyakan perlakuan yg mau dilakukan visitor thd hewan
    }

    public void hunting(){
        System.out.println("Lion is hunting..");
        makeVoice("err...!");
    }

    public void brushmane(){
        System.out.println("Clean the lion's mane..");
        makeVoice("Hauhhmm!");
    }

    public void disturb(){
        makeVoice("HAUHHMM!");
    }

    public void action(){
        Scanner input = new Scanner(System.in);
        int masukan = Integer.parseInt(input.nextLine());
        if (masukan == 1) hunting();
        else if (masukan == 2) brushmane();
        else if (masukan == 3) disturb();
        else doNothing(); 
    }
}